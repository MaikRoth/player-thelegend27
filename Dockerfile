FROM maven:3.8.7-openjdk-18-slim AS build
ARG GAME_HOST
ARG RABBITMQ_USERNAME
ARG RABBITMQ_PASSWORD
ARG RABBITMQ_PORT
ARG RABBITMQ_HOST
# Set the working directory to the root of the project
WORKDIR /app
# Copy the POM file to download dependencies
COPY pom.xml .
# Download dependencies
RUN mvn dependency:go-offline
# Copy the source code and build the application
COPY . .
RUN mvn package -DGAME_HOST=$GAME_HOST -DRABBITMQ_USERNAME=$RABBITMQ_USERNAME -DRABBITMQ_PASSWORD=$RABBITMQ_PASSWORD -DRABBITMQ_PORT=$RABBITMQ_PORT -DRABBITMQ_HOST=$RABBITMQ_HOST

# Use an official JDK image as runtime stage
FROM eclipse-temurin:18
ARG GAME_HOST
ARG RABBITMQ_USERNAME
ARG RABBITMQ_PASSWORD
ARG RABBITMQ_PORT
ARG RABBITMQ_HOST
# Set the working directory to the root of the filesystem
WORKDIR /
# Copy the built JAR file to the root of the filesystem and rename it
COPY --from=build /app/target/*-jar-with-dependencies.jar ./app.jar

# Set environment variables for the application
ENV GAME_HOST=$GAME_HOST
ENV RABBITMQ_USERNAME=$RABBITMQ_USERNAME
ENV RABBITMQ_PASSWORD=$RABBITMQ_PASSWORD
ENV RABBITMQ_PORT=$RABBITMQ_PORT
ENV RABBITMQ_HOST=$RABBITMQ_HOST

# Expose the port that the application listens on
EXPOSE 8090

# Start the application
CMD ["java", "-jar", "./app.jar"]
