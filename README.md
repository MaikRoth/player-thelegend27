# Authors: Maik Roth, Christoph Müller, Andre Müller
TheLegend27 is a Bot written for the Microservice Dungeon game.

## How does the bot work?
The bot waits for a game to register and join to and starts to listen to incoming events.
The events are being mapped to DTOs and being sent into a corresponding channel. Each Event that we work with has it's own channel.
For each event type that we use we have a Handler method which contains the logic of processing the event.
Events are processed in FIFO. Once a game is over the repositories get cleared and the application goes in a state where it is waiting for a new game to be created.

The application loop is found in the Game/GameStateHandler Class.
This class is responsible for the flow of our application.

## Commands
After 60% of the round time to send commands are over we start to send out commands for each robot to ensure our internal state of the game is fresh (by ensuring that most or all events are processed).

## Strategies
Strategies are assigned when a robot spawns (see RobotSpawnedHandler).
Farmers manage their money by themselves. They have to pay Taxes tho each time they sell their inventory. When they're fully upgraded all the money is being sent to the UpgradeManager.
Fighters request upgrades at the UpgradeManager. If enough money is leftover they're allowed to buy Upgrades.
If all Fighters are fully upgraded new robots are being bought.

## P.S.
Keep in mind that this application is multithreaded with Kotlin Coroutines so we had to coordinate the read/write accesses to the robots/planets via Mutexes.
