package com.example.thelegend27.player.application.usecases

import com.example.thelegend27.eventinfrastructure.trading.BankAccountInitializedEvent
import com.example.thelegend27.player.domain.Player
import org.slf4j.LoggerFactory

class BankAccountInitializedHandler {
    private val logger = LoggerFactory.getLogger(BankAccountInitializedHandler::class.java)
    suspend fun handle(event: BankAccountInitializedEvent) {
        Player.changeMoneten(event.balance.toDouble())
        logger.info("Player Moneten initialized to ${event.balance.toDouble()}")
    }
}