package com.example.thelegend27.player.domain

import kotlinx.coroutines.*


object Player {
    val name: String = "TheLegend27"
    val email: String = "andremuller562@gmail.com"
    lateinit var playerQueue: PlayerQueue
    lateinit var playerId: String

    @OptIn(DelicateCoroutinesApi::class)
    val monentenAccessThread = newSingleThreadContext("MonetenAccessThread")
    var moneten: Number = 0.0

    suspend fun getMoneten(): Number {
        return withContext(monentenAccessThread) {
            moneten
        }
    }

    suspend fun changeMoneten(amount: Double) {
        withContext(monentenAccessThread) {
            moneten = amount
        }
    }

}