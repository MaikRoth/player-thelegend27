package com.example.thelegend27.robot.application

import com.example.thelegend27.eventinfrastructure.Channels
import com.example.thelegend27.game.application.GameClient
import com.example.thelegend27.planet.application.PlanetService
import com.example.thelegend27.planet.domain.DiscoveredPlanet
import com.example.thelegend27.planet.domain.PlanetRepository
import com.example.thelegend27.robot.application.usecases.*
import com.example.thelegend27.robot.domain.EnemyRobot
import com.example.thelegend27.robot.domain.EnemyRobotRepository
import com.example.thelegend27.robot.domain.Robot
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.strategies.RobotStrategy
import com.example.thelegend27.robot.domainprimitives.EnemyRobotEntryDetailed
import com.example.thelegend27.robot.domainprimitives.EnemyRobotEntryMinimal
import com.example.thelegend27.robot.domainprimitives.FriendlyRobotEntryDetailed
import com.example.thelegend27.robot.domainprimitives.FriendlyRobotEntryMinimal
import com.example.thelegend27.trading.domain.UpgradeManager
import com.example.thelegend27.utility.throwables.EntryDoesNotExistException
import io.ktor.client.call.*
import kotlinx.coroutines.*
import kotlinx.coroutines.selects.select
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.slf4j.LoggerFactory
import java.util.*

@OptIn(DelicateCoroutinesApi::class)
object RobotService {
    private val logger = LoggerFactory.getLogger(RobotService::class.java)
    private val mutex = Mutex()
    private val planets = PlanetRepository
    private val robotSpawnedChannel = Channels.robotSpawned
    private val robotMovedChannel = Channels.robotMoved
    private val robotAttackedChannel = Channels.robotAttackedIntegrationEventChannel
    private val robotEnergyUpdatedChannel = Channels.robotEnergyUpdated
    private val robotHealthUpdatedChannel = Channels.robotHealthUpdated
    private val robotsRevealedChannel = Channels.robotsRevealedIntegrationEventChannel
    private val robotUpgradedChannel = Channels.robotUpgraded
    private val robotResourceMinedIntegrationEventChannel = Channels.robotResourceMinedIntegrationEventChannel
    private val robotResourceRemovedChannel = Channels.robotResourceRemovedIntegrationEventChannel

    private val robotSpawnedHandlerWorker = newSingleThreadContext("RobotSpawnedHandler")
    private val robotMovedHandlerWorker = newSingleThreadContext("RobotMovedHandler")
    private val robotAttackedHandlerWorker = newSingleThreadContext("RobotAttackedHandler")
    private val robotEnergyOrHealthUpdatedHandlerWorker = newSingleThreadContext("RobotEnergyUpdatedHandler")
    private val robotsRevealedHandlerWorker = newSingleThreadContext("RobotsRevealedHandler")
    private val robotUpgradedHandlerWorker = newSingleThreadContext("RobotUpgradedHandler")
    private val robotResourceMinedOrRemovedHandlerWorker = newSingleThreadContext("RobotResourceMinedHandler")


    @OptIn(ExperimentalCoroutinesApi::class)
    private suspend fun startListeningForPlanetUpdates() {
        CoroutineScope(Dispatchers.IO).launch {
            planets.asFlow().collect { planetMap ->
                mutex.withLock {
                    val jobs = mutableListOf<Job>()
                    val robots = RobotRepository.getAll()
                    robots.forEach { robot ->
                        jobs += launch {
                            val currentPlanet = planetMap[robot.currentPlanet.id]
                            if (currentPlanet != null && currentPlanet is DiscoveredPlanet) {
                                robot.mutex.withLock {
                                    logger.info("UPDATING ROBOT ${robot.id} FROM ${robot.currentPlanet} TO PLANET ${currentPlanet} ")
                                    robot.currentPlanet = currentPlanet
                                    RobotRepository.addOrReplace(robot)
                                }
                            }
                        }
                        jobs.joinAll()
                    }
                }
                delay(5000)
            }
        }
    }

    suspend fun handleRobotEvents() {
        CoroutineScope(Dispatchers.IO).launch {
            startListeningForPlanetUpdates()
            while (true) {
                select<Unit> {
                    robotSpawnedChannel.onReceive { event ->
                        launch(robotSpawnedHandlerWorker) {
                            mutex.withLock {
                                RobotSpawnedHandler().handle(event.eventBody)
                            }
                        }
                    }

                    robotEnergyUpdatedChannel.onReceive { event ->
                        launch(robotEnergyOrHealthUpdatedHandlerWorker) {
                            mutex.withLock {
                                RobotEnergyUpdatedHandler().handle(event.eventBody)
                            }
                        }
                    }
                    robotHealthUpdatedChannel.onReceive { event ->
                        launch(robotEnergyOrHealthUpdatedHandlerWorker) {
                            mutex.withLock {
                                RobotHealthUpdatedHandler().handle(event.eventBody)
                            }

                        }
                    }

                    robotMovedChannel.onReceive { event ->
                        launch(robotMovedHandlerWorker) {
                            mutex.withLock {
                                RobotMovedHandler().handle(event.eventBody)
                            }
                        }
                    }
                    robotAttackedChannel.onReceive { event ->
                        launch(robotAttackedHandlerWorker) {
                            mutex.withLock {
                                RobotAttackedHandler().handle(event.eventBody)
                            }

                        }
                    }
                    robotUpgradedChannel.onReceive { event ->
                        launch(robotUpgradedHandlerWorker) {
                            mutex.withLock {
                                RobotUpgradedHandler().handle(event.eventBody)
                            }

                        }
                    }
                    robotResourceRemovedChannel.onReceive { event ->
                        launch(robotResourceMinedOrRemovedHandlerWorker) {
                            mutex.withLock {
                                RobotResourceRemovedHandler().handle(event.eventBody)
                            }
                        }
                    }
                    robotResourceMinedIntegrationEventChannel.onReceive { event ->
                        launch(robotResourceMinedOrRemovedHandlerWorker) {
                            mutex.withLock {
                                RobotResourceMinedHandler().handle(event.eventBody)
                            }

                        }
                    }
                    robotsRevealedChannel.onReceive { event ->
                        launch(robotsRevealedHandlerWorker) {
                            mutex.withLock {
                                RobotsRevealedHandler().handle(event.eventBody)
                            }

                        }
                    }
                }
            }
        }
    }

    fun getAllRobots(): List<Robot> {
        return RobotRepository.getAll()
    }

    fun addOrReplaceRobot(robot: Robot) {
        RobotRepository.addOrReplace(robot)
    }

    fun removeRobot(robot: Robot) {
        RobotRepository.removeElement(robot)
    }

    fun getAllRobotsOnPlanet(planetId: UUID): List<Robot> {
        return getAllRobots().filter { robot -> robot.currentPlanet.id == planetId }
    }

    fun isEnemyOnPlanetWithId(planetId: UUID): Boolean {
        return EnemyRobotRepository.getAll().any { it.planetId == planetId }
    }

    fun getAmountOfOurRobots(): Int = RobotRepository.getSize()

    fun getAllEnemiesOnPlanet(planetId: UUID): List<EnemyRobot> {
        return EnemyRobotRepository.getAll().filter { it.planetId == planetId }
    }

    suspend fun getAllEnemyRobotsInClusterByPlanetId(planetId: UUID): List<EnemyRobot> {
        val clusterId = PlanetService.getPlanetById(planetId).getOrElse { return emptyList() }.clusterId
        val cluster = PlanetService.getClusterByClusterId(clusterId).getOrElse { return emptyList() }
        val enemyRobots = EnemyRobotRepository.getAll()
        return enemyRobots.filter { enemyRobot -> cluster.getPlanetIds.contains(enemyRobot.planetId) }
    }

    /**
     * Clears all robots from the repository so that the next game can start with a clean state.
     * Also resets the upgrade manager, so that the next game can start with the default upgrades.
     */
    suspend fun clear() {
        logger.info("Clearing RobotService")
        RobotRepository.clear()
        EnemyRobotRepository.clear()
        UpgradeManager.clear()
    }


    suspend fun callCommandsForEachRobotParallel() {
        mutex.withLock {
            val robots = RobotRepository.getAll()
            withContext(Dispatchers.IO) {
                robots.forEach { robot ->
                    launch {
                        robot.mutex.withLock {
                            val command = robot.strategy.getCommand()!!
                            try {
                                logger.info("Command for Robot :\n $command \n Current planet of robot : ${robot.currentPlanet.id} Strategy is ${robot.strategy::class.java.simpleName}")
                                GameClient.sendCommand(command)
                            } catch (e: Exception) {
                                logger.info(
                                    "Sending Command to Game Service failed!${e.message}\n" + "Command that was tried to be sent :\n$command\n" + "Response from Game Service :\n$e"
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    suspend fun switchRobotStrategy(robotId: UUID, strategy: RobotStrategy) {
        val robotResult = RobotRepository.get(robotId)
        robotResult.onSuccess { robot ->
            robot.mutex.withLock {
                robot.strategy = strategy
                RobotRepository.addOrReplace(robot)
            }
        }

    }


    fun getAllRobotsAsFriendlyRobotEntryMinimal(): List<FriendlyRobotEntryMinimal> {
        return getAllRobots().map { FriendlyRobotEntryMinimal.fromRobot(it) }
    }

    fun getAllRobotsAsFriendlyRobotEntryDetailed(): List<FriendlyRobotEntryDetailed> {
        return getAllRobots().map { FriendlyRobotEntryDetailed.fromRobot(it) }
    }

    fun getMinimalFriendlyRobotEntryById(id: UUID): Result<FriendlyRobotEntryMinimal> {
        RobotRepository.get(id).onSuccess {
            return Result.success(FriendlyRobotEntryMinimal.fromRobot(it))
        }.onFailure {
            return Result.failure(it)
        }
        return Result.failure(EntryDoesNotExistException("Entry of Id $id does not exist"))
    }

    fun getDetailedFriendlyRobotEntryById(id: UUID): Result<FriendlyRobotEntryDetailed> {
        RobotRepository.get(id).onSuccess {
            return Result.success(FriendlyRobotEntryDetailed.fromRobot(it))
        }.onFailure {
            return Result.failure(it)
        }
        return Result.failure(EntryDoesNotExistException("Entry of Id $id does not exist"))
    }

    fun getAllEnemyRobotsAsEnemyRobotEntryMininmal(): List<EnemyRobotEntryMinimal> {
        return EnemyRobotRepository.getAll().map { EnemyRobotEntryMinimal.fromEnemyRobot(it) }
    }

    fun getEnemyRobotEntryMinimalById(id: UUID): Result<EnemyRobotEntryMinimal> {
        EnemyRobotRepository.get(id).onSuccess {
            return Result.success(EnemyRobotEntryMinimal.fromEnemyRobot(it))
        }.onFailure {
            return Result.failure(it)
        }
        return Result.failure(EntryDoesNotExistException("Entry of Id $id does not exist"))
    }

    fun getEnemyRobotEntryDetailedById(id: UUID): Result<EnemyRobotEntryDetailed> {
        EnemyRobotRepository.get(id).onSuccess {
            return Result.success(EnemyRobotEntryDetailed.fromEnemyRobot(it))
        }.onFailure {
            return Result.failure(it)
        }
        return Result.failure(EntryDoesNotExistException("Entry of Id $id does not exist"))
    }

    fun isFriendlyRobotOnPlanet(planetId: UUID): Boolean {
        return RobotRepository.getAll().any { it.currentPlanet.id == planetId }
    }

    fun amountOfFriendlyRobotsOnPlanet(planetId: UUID): Int {
        return RobotRepository.getAll().count { it.currentPlanet.id == planetId }
    }


}

