package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.Channels
import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotAttackedIntegrationEvent
import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotBattleInfoDto
import com.example.thelegend27.robot.application.RobotService
import com.example.thelegend27.robot.domain.EnemyRobot
import com.example.thelegend27.robot.domain.EnemyRobotRepository
import com.example.thelegend27.robot.domain.Robot
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.strategies.farming.FarmStrategy
import com.example.thelegend27.robot.domainprimitives.CurrentStatus
import com.example.thelegend27.trading.domain.UpgradeManager
import kotlinx.coroutines.sync.withLock
import org.slf4j.LoggerFactory
import java.util.*

class RobotAttackedHandler {
    private val logger = LoggerFactory.getLogger(RobotAttackedHandler::class.java)
    suspend fun handle(event: RobotAttackedIntegrationEvent) {
        val robotId = UUID.fromString(event.target.robotId)
        val robotResult = RobotRepository.get(robotId)
        robotResult.onSuccess { robot ->
            handleOurRobot(robot, event.target)
        }
        robotResult.onFailure {
            handleEnemyRobot(event.target, event.attacker)
        }

    }

    private suspend fun handleOurRobot(ourRobot: Robot, target: RobotBattleInfoDto) {
        ourRobot.mutex.withLock {
            logger.info("===Our Robot with id ${ourRobot.id} just got attacked by ${target.robotId}! Health: ${target.availableHealth} Energy: ${target.availableEnergy}===")
            ourRobot.alive = target.alive
            ourRobot.currentStatus = CurrentStatus(target.availableHealth, target.availableEnergy)
            UpgradeManager.removeFighterFromMaxLevelList(ourRobot.id)
            if (!ourRobot.alive) {
                logKilledMessage(ourRobot)
                sendMonetenMadeToUpgradeManagerIfFarmer(ourRobot)
                RobotService.removeRobot(ourRobot)
            } else {
                RobotService.addOrReplaceRobot(ourRobot)
            }
        }
    }

    private suspend fun handleEnemyRobot(target: RobotBattleInfoDto, attacker: RobotBattleInfoDto) {
        val enemyRobotResult = EnemyRobotRepository.get(UUID.fromString(target.robotId))
        enemyRobotResult.onSuccess { enemyRobot ->
            enemyRobot.mutex.withLock {
                enemyRobot.health = target.availableHealth
                enemyRobot.energy = target.availableEnergy
                if (enemyRobot.health <= 0) {
                    logEnemyKilledMessage(enemyRobot, attacker)
                    EnemyRobotRepository.removeElement(enemyRobot)
                    return
                }
                EnemyRobotRepository.addOrReplace(enemyRobot)
            }
        }
    }

    private fun logEnemyKilledMessage(robot: EnemyRobot, attacker: RobotBattleInfoDto) {
        val robotId = robot.robotId
        val fightingScore = robot.calculateFightingScore()
        val message = """
| ------------------------------------------------------------------------------------------             
| ENEMY Robot $robotId just got killed by our robot ${attacker.robotId} Health : ${attacker.availableHealth} Energy :${attacker.availableEnergy} died!                                                                 |
| Fighting score : $fightingScore                                                           |
| ------------------------------------------------------------------------------------------
"""
        logger.info(message)
    }

    private suspend fun sendMonetenMadeToUpgradeManagerIfFarmer(robot: Robot) {
        if (robot.strategy is FarmStrategy) {
            val channelForLeftoverMoney = Channels.channelForLeftoverMoney
            val monetenMade = (robot.strategy as FarmStrategy).monetenMade
            if (monetenMade > 0) {
                logger.info(
                    """
                    | ------------------------------------------------------------------------------------------------------------------------             
                    | Robot ${robot.id} was a farmer that died and had $monetenMade $ left over! Sending $monetenMade to UpgradeManager                      
                    | ------------------------------------------------------------------------------------------------------------------------
                    """
                )
                channelForLeftoverMoney.send(monetenMade)
            }
        }
    }

    private fun logKilledMessage(robot: Robot) {
        val robotId = robot.id
        val strategy = robot.strategy.javaClass.simpleName
        val fightingScore = robot.calculateFightingScore()
        val message = """
| ------------------------------------------------------------------------------------------             
| Robot $robotId just died!                                                                 |
| It was a $strategy                                                                        |
| Fighting score : $fightingScore                                                           |
| ------------------------------------------------------------------------------------------
"""
        logger.info(message)
    }

}