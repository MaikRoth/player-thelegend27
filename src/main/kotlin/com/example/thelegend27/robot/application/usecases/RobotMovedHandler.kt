package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.RobotMovedEvent
import com.example.thelegend27.planet.application.PlanetService
import com.example.thelegend27.robot.application.RobotService
import com.example.thelegend27.robot.domain.RobotRepository
import kotlinx.coroutines.sync.withLock
import org.slf4j.LoggerFactory
import java.util.*

class RobotMovedHandler {
    private val logger = LoggerFactory.getLogger(RobotMovedHandler::class.java)
    suspend fun handle(event: RobotMovedEvent) {
        val robotResult = RobotRepository.get(UUID.fromString(event.robotId))
        robotResult.onSuccess { robot ->
            robot.mutex.withLock {
                val result2 = PlanetService.getPlanetById(UUID.fromString(event.toPlanet))
                result2.onSuccess { planet ->
                    logRobotMovement(robot.id, robot.currentPlanet.id, planet.id)
                    robot.currentPlanet = planet
                    RobotService.addOrReplaceRobot(robot)
                }
                result2.onFailure {
                    logger.error("RobotMoved failed! Planet: ${event.toPlanet} not found")
                }
            }
        }
        robotResult.onFailure {
            logger.error("RobotMoved failed! Robot: ${event.robotId} not found")
        }
    }

    private fun logRobotMovement(robotId: UUID, fromPlanet: UUID, toPlanet: UUID) {
        logger.info(
            """
                |---------------------------------------------------------|
                |Robot $robotId moved from $fromPlanet to $toPlanet|
                |---------------------------------------------------------|
            """.trimIndent()
        )
    }

}