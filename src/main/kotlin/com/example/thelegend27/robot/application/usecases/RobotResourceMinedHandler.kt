package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotResourceMinedIntegrationEvent
import com.example.thelegend27.robot.application.RobotService
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domainprimitives.Inventory
import com.example.thelegend27.utility.throwables.EntryDoesNotExistException
import kotlinx.coroutines.sync.withLock
import org.slf4j.LoggerFactory
import org.slf4j.MarkerFactory
import java.util.*

class RobotResourceMinedHandler {
    private val logger = LoggerFactory.getLogger(RobotResourceMinedHandler::class.java)

    suspend fun handle(event: RobotResourceMinedIntegrationEvent) {
        val robotResult = RobotRepository.get(UUID.fromString(event.robotId))

        robotResult.onSuccess { robot ->
            robot.mutex.withLock {
                val robotInventoryBefore = robot.inventory.usedStorage
                val robotInventoryAfter = robot.inventory.fromInventoryAndRobotResourceMined(event)
                if (robotInventoryAfter.usedStorage != 0 && robotInventoryBefore > robotInventoryAfter.usedStorage) {
                    logger.error(
                        MarkerFactory.getMarker("RACECONDITION"),
                        "Robot ${robot.id} just got less inventory than he had before, probably Race Condition ${robot.inventory.resources} MAX STORAGE :${robot.inventory.maxStorage} USED STORAGE :${robot.inventory.usedStorage} Before : $robotInventoryBefore"
                    )
                } else {
                    logRobotInventoryChange(robot.id, robot.inventory, robotInventoryAfter)
                    robot.inventory = robotInventoryAfter
                    RobotService.addOrReplaceRobot(robot)
                }
            }
        }
        robotResult.onFailure { throw EntryDoesNotExistException("handleRobotResourceMined: Robot with id ${event.robotId} doesnt not exist!") }
    }

    private fun logRobotInventoryChange(robotId: UUID, oldInventory: Inventory, newInventory: Inventory) {
        logger.info(
            """
                   |---------------------------------------------------------|
                   |Robot with id $robotId just Mined something.
                   |Max Storage (Old) : ${oldInventory.maxStorage}, Used Storage(Old) : ${oldInventory.usedStorage}
                   |Max Storage (New) : ${newInventory.maxStorage}, Used Storage(New) : ${newInventory.usedStorage}
                   |Inventory before:
                    $oldInventory
                   |Inventory after:
                    $newInventory
                   |---------------------------------------------------------|
                    
                """.trimIndent()
        )
    }
}