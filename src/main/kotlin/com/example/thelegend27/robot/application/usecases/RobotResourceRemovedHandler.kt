package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotResourceRemovedIntegrationEvent
import com.example.thelegend27.robot.application.RobotService
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domainprimitives.Inventory
import kotlinx.coroutines.sync.withLock
import org.slf4j.LoggerFactory
import java.util.*


class RobotResourceRemovedHandler {
    private val logger = LoggerFactory.getLogger(RobotResourceRemovedHandler::class.java)
    suspend fun handle(event: RobotResourceRemovedIntegrationEvent) {
        val robotId = UUID.fromString(event.robotId)
        val robotResult = RobotRepository.get(robotId)
        robotResult.onSuccess { robot ->
            robot.mutex.withLock {
                val newInventory = robot.inventory.fromInventoryAndRobotResourceRemoved(event)
                logResourceRemoved(robotId, robot.inventory, newInventory)
                robot.inventory = newInventory
                RobotService.addOrReplaceRobot(robot)
            }
        }
    }

    private fun logResourceRemoved(robotId: UUID, oldInventory: Inventory, newInventory: Inventory) {
        logger.info(
            """
            |---------------------------------------------------------|
            |Robot $robotId just sold his inventory.
            |Inventory Old:
            $oldInventory
            |Inventory New:
            $newInventory
            |---------------------------------------------------------|
        """.trimIndent()
        )
    }

}