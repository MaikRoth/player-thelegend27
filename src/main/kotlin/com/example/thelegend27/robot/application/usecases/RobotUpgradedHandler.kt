package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.RobotUpgradedEvent
import com.example.thelegend27.robot.application.RobotService
import com.example.thelegend27.robot.domain.RobotRepository
import kotlinx.coroutines.sync.withLock
import org.slf4j.LoggerFactory
import java.util.*

class RobotUpgradedHandler {
    private val logger = LoggerFactory.getLogger(RobotUpgradedHandler::class.java)

    suspend fun handle(event: RobotUpgradedEvent) {
        val robotResult = RobotRepository.get(UUID.fromString(event.robotId))

        robotResult.onSuccess { robot ->
            robot.mutex.withLock {
                robot.levelUp(event.upgradeType)
                logRobotUpgraded(robot.id, event.upgradeType.toString(), event.level)
                logger.info("Robot ${robot.id} upgraded ${event.upgradeType} to Level ${event.level}")
                RobotService.addOrReplaceRobot(robot)
            }
        }
        robotResult.onFailure {
            logger.error("RobotUpgraded failed! Robot: ${event.robotId} not found")
        }
    }

    private fun logRobotUpgraded(robotId: UUID, upgradeType: String, level: Int) {
        logger.info(
            """
                
            |---------------------------------------------------------|
            |Robot $robotId upgraded $upgradeType from level ${level - 1} to Level $level|
            |---------------------------------------------------------|
            
            """.trimIndent()
        )
    }

}