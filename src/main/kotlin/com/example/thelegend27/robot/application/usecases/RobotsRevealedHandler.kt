package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotsRevealedIntegrationEvent
import com.example.thelegend27.robot.domain.EnemyRobot
import com.example.thelegend27.robot.domain.EnemyRobotRepository
import com.example.thelegend27.robot.domain.RobotRepository
import org.slf4j.LoggerFactory
import java.util.*

class RobotsRevealedHandler {
    private val logger = LoggerFactory.getLogger(RobotsRevealedHandler::class.java)

    fun handle(event: RobotsRevealedIntegrationEvent) {
        val enemyRobots: List<EnemyRobot> =
            event.robots.map { it.toEnemyRobot() }.filter { robot ->
                !RobotRepository.containsKey(UUID.fromString(robot.robotId.toString()))
            }

        enemyRobots.forEach { robot -> EnemyRobotRepository.addOrReplace(robot) }

        val robotsToDelete = EnemyRobotRepository.getAll().filter { robot ->
            !enemyRobots.any { it.robotId == robot.robotId }
        }
        robotsToDelete.forEach {
            EnemyRobotRepository.removeElement(it)
            logEnemyKilledRobot(it)
        }

    }

    private fun logEnemyKilledRobot(killedRobot: EnemyRobot) {
        logger.info(
            """
            |---------------------------------------------------------|
            | Enemy Robot ${killedRobot.robotId} got killed by someone else!|
            | Their Fighting score was: ${killedRobot.calculateFightingScore()} | 
            |---------------------------------------------------------|
        """.trimIndent()
        )
    }
}