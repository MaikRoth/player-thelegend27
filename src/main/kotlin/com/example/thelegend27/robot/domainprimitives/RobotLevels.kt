package com.example.thelegend27.robot.domainprimitives


data class RobotLevels(
    var healthLevel: Int = 0,
    var damageLevel: Int = 0,
    var miningSpeedLevel: Int = 0,
    var miningLevel: Int = 0,
    var energyLevel: Int = 0,
    var energyRegenLevel: Int = 0,
    var storageLevel: Int = 0,
) {
    companion object {
        val MAXIMUM_LEVEL = 5
    }

    init {
        checkLevel(healthLevel)
        checkLevel(damageLevel)
        checkLevel(miningSpeedLevel)
        checkLevel(miningLevel)
        checkLevel(energyLevel)
        checkLevel(energyRegenLevel)
        checkLevel(storageLevel)
    }

    private fun checkLevel(level: Int) {
        if (level > MAXIMUM_LEVEL) {
            throw IllegalArgumentException("Level cannot be greater than $MAXIMUM_LEVEL")
        }
    }
    override fun toString(): String {
        return "RobotLevels(healthLevel=$healthLevel, damageLevel=$damageLevel, miningSpeedLevel=$miningSpeedLevel, miningLevel=$miningLevel, energyLevel=$energyLevel, energyRegenLevel=$energyRegenLevel, storageLevel=$storageLevel)"
    }


    fun sumOfLevels(): Int {
        return healthLevel + damageLevel + miningSpeedLevel + miningLevel + energyLevel + energyRegenLevel + storageLevel
    }

    fun upgradeHealth() {
        if (this.healthLevel >= MAXIMUM_LEVEL) {
            throw IllegalArgumentException("Health level cannot be greater than 5")
        }
        healthLevel++
    }

    fun upgradeAttack() {
        if (this.damageLevel >= MAXIMUM_LEVEL) {
            throw IllegalArgumentException("Attack level cannot be greater than 5")
        }
        damageLevel++
    }

    fun upgradeMiningSpeed() {
        if (this.miningSpeedLevel >= MAXIMUM_LEVEL) {
            throw IllegalArgumentException("Mining speed level cannot be greater than 5")
        }
        miningSpeedLevel++
    }

    fun upgradeMining() {
        if (this.miningLevel >= MAXIMUM_LEVEL) {
            throw IllegalArgumentException("Mining level cannot be greater than 5")
        }
        miningLevel++
    }

    fun upgradeEnergy() {
        if (this.energyLevel >= MAXIMUM_LEVEL) {
            throw IllegalArgumentException("Energy level cannot be greater than 5")
        }
        energyLevel++
    }

    fun upgradeEnergyRegen() {
        if (this.energyRegenLevel > MAXIMUM_LEVEL) {
            throw IllegalArgumentException("Energy regen level cannot be greater than 5")
        }
        energyRegenLevel++

    }

    fun upgradeStorage() {
        if (this.storageLevel >= MAXIMUM_LEVEL) {
            throw IllegalArgumentException("Storage level cannot be greater than 5")
        }
        storageLevel++
    }

    fun isLevelZero(): Boolean {
        return healthLevel == 0 && damageLevel == 0 && miningSpeedLevel == 0 && miningLevel == 0 && energyLevel == 0 && energyRegenLevel == 0 && storageLevel == 0
    }
}