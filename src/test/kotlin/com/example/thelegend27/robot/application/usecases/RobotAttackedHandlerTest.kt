import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotAttackedIntegrationEvent
import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotBattleInfoDto
import com.example.thelegend27.robot.application.usecases.RobotAttackedHandler
import com.example.thelegend27.robot.domain.EnemyRobotRepository
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.TestHelper
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*

class RobotAttackedHandlerTest {
    private val testHelper = TestHelper()
    private var handler: RobotAttackedHandler = RobotAttackedHandler()

    @Test
    fun `when our robot gets attacked it should update our robot`() {
        val robot = testHelper.getRobot()
        RobotRepository.add(robot)

        val healthAfterAttack = robot.currentStatus.health-2

        val attacker = RobotBattleInfoDto(UUID.randomUUID().toString(), 100, 100, true)
        val target = RobotBattleInfoDto(robot.id.toString(), healthAfterAttack, robot.currentStatus.energy, true)
        val event = RobotAttackedIntegrationEvent(target = target, attacker = attacker)

        runBlocking {
            handler.handle(event)
        }

        val updatedRobot = RobotRepository.get(robot.id).getOrThrow()
        assertEquals(healthAfterAttack, updatedRobot.currentStatus.health)
        assertTrue(updatedRobot.alive)
    }

    @Test
    fun `when our robot gets attacked and dies it should remove our robot from the repository`() {
        val robot = testHelper.getRobot()
        RobotRepository.add(robot)

        val attacker = RobotBattleInfoDto(UUID.randomUUID().toString(), 100, 100, true)
        val target = RobotBattleInfoDto(robot.id.toString(), 0, robot.currentStatus.energy, false)
        val event = RobotAttackedIntegrationEvent(target = target, attacker = attacker)

        runBlocking {
            handler.handle(event)
        }

        assertFalse(RobotRepository.contains(robot))
    }

    @Test
    fun `if we attack a enemy robot the enemy robot should get updated`() {
        val enemyRobot = testHelper.getEnemyRobot()
        EnemyRobotRepository.add(enemyRobot)

        val attacker = RobotBattleInfoDto(UUID.randomUUID().toString(),100, 100, true)
        val target = RobotBattleInfoDto(enemyRobot.robotId.toString(), (enemyRobot.health-2), enemyRobot.energy, true)

        val event = RobotAttackedIntegrationEvent(target = target, attacker = attacker)
        runBlocking {
            handler.handle(event)
        }

        val updatedEnemyRobot = EnemyRobotRepository.get(enemyRobot.robotId).getOrThrow()
        assertEquals(enemyRobot.health, updatedEnemyRobot.health)
        assertTrue(updatedEnemyRobot.health > 0)

    }

    @Test
    fun `if we attack and kill a enemy robot the enemy robot should get deleted from Repository`() {
        val enemyRobot = testHelper.getEnemyRobot()
        EnemyRobotRepository.add(enemyRobot)

        val attacker = RobotBattleInfoDto(UUID.randomUUID().toString(),100, 100, true)
        val target = RobotBattleInfoDto(enemyRobot.robotId.toString(), 0, enemyRobot.energy, false)

        val event = RobotAttackedIntegrationEvent(target = target, attacker = attacker)
        runBlocking {
            handler.handle(event)
        }

        assertFalse(EnemyRobotRepository.contains(enemyRobot))

    }
}