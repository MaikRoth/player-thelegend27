package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.RobotEnergyUpdatedEvent
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.TestHelper
import com.example.thelegend27.robot.domainprimitives.CurrentStatus
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RobotEnergyUpdatedHandlerTest {
    val testHelper = TestHelper()

    @Test
    fun `handle - robot found`() {
        runBlocking {
            // Arrange
            val robot = testHelper.getRobotFromDto(testHelper.getRobotEvent())
            RobotRepository.add(robot)

            val energyUpdatedEvent = RobotEnergyUpdatedEvent(
                robot.id.toString(),
                1, 50)

            // Act
            val handler = RobotEnergyUpdatedHandler()
            handler.handle(energyUpdatedEvent)

            // Assert
            assertEquals(
                CurrentStatus(robot.currentStatus.health, 50),
                RobotRepository.get(robot.id).getOrThrow().currentStatus
            )
        }
    }
}