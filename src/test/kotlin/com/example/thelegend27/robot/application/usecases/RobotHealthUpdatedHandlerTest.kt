package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.RobotHealthUpdatedEvent
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.TestHelper
import com.example.thelegend27.robot.domainprimitives.CurrentStatus
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class RobotHealthUpdatedHandlerTest {
    val testHelper = TestHelper()
    @Test
    fun `handle - robot found`() {
        runBlocking {
            // Arrange
            val robot = testHelper.getRobotFromDto(testHelper.getRobotEvent())
            RobotRepository.add(robot)

            val healthUpdatedEvent = RobotHealthUpdatedEvent(
                robot.id.toString(),
                1, 50)

            // Act
            val handler = RobotHealthUpdatedHandler()
            handler.handle(healthUpdatedEvent)

            // Assert
            assertEquals(
                CurrentStatus(50, robot.currentStatus.energy),
                RobotRepository.get(robot.id).getOrThrow().currentStatus
            )
        }
    }
}