package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.RobotMovedEvent
import com.example.thelegend27.planet.domain.PlanetRepository
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.TestHelper
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

class RobotMovedHandlerTest {
    val testHelper = TestHelper()

    @Test
    fun handle() {
        // Arrange
        val robot = testHelper.getRobotFromDto(testHelper.getRobotEvent())
        RobotRepository.add(robot)

        val toPlanet = testHelper.createDiscoveredPlanet()
        PlanetRepository.add(toPlanet)

        val event = RobotMovedEvent(robot.id.toString(), robot.currentPlanet.id.toString(), toPlanet.id.toString())

        // Act
        runBlocking {
            RobotMovedHandler().handle(event)
        }

        // Assert
        val updatedRobot = RobotRepository.get(robot.id).getOrThrow()
        assertEquals(toPlanet, updatedRobot.currentPlanet)
    }
}