package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.ResourceInventoryDto
import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotResourceMinedIntegrationEvent
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.TestHelper
import com.example.thelegend27.trading.domain.Resource
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class RobotResourceMinedHandlerTest {
    val testHelper  = TestHelper()
    @Test
    fun handle() {
        // Given
        val robot = testHelper.getRobotFromDto(testHelper.getRobotEvent())
        val event = RobotResourceMinedIntegrationEvent(robot.id.toString(), 1, Resource.COAL, ResourceInventoryDto(1,0,0,0,0))
        RobotRepository.add(robot)
        val robotResourceMinedHandler = RobotResourceMinedHandler()

        // When
        runBlocking {
            robotResourceMinedHandler.handle(event)
        }
        // Then
        assertEquals(robot, RobotRepository.get(robot.id).getOrThrow())
        assertEquals(1, RobotRepository.get(robot.id).getOrThrow().inventory.resources[Resource.COAL])
        assertEquals(1, RobotRepository.get(robot.id).getOrThrow().inventory.usedStorage)
    }
}