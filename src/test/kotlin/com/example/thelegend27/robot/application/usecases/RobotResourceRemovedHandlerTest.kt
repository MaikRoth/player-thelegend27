import com.example.thelegend27.eventinfrastructure.robot.ResourceInventoryDto
import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotResourceRemovedIntegrationEvent
import com.example.thelegend27.robot.application.usecases.RobotResourceRemovedHandler
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.TestHelper
import com.example.thelegend27.trading.domain.Resource
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RobotResourceRemovedHandlerTest {
    val testHelper = TestHelper()
    @Test
    fun handle_RobotExists_ResourceRemoved() = runBlocking {
        // Arrange
        val robot = testHelper.getRobotFromDto(testHelper.getRobotEvent())
        robot.inventory = testHelper.inventoryFilledWithResources()
        RobotRepository.add(robot)

        val event = RobotResourceRemovedIntegrationEvent(robotId = robot.id.toString(), 1, Resource.COAL, ResourceInventoryDto(0,0,0,0,0))
        val handler = RobotResourceRemovedHandler()

        // Act
        handler.handle(event)

        // Assert
        val updatedRobot = RobotRepository.get(robot.id).getOrThrow()
        assertEquals(0, updatedRobot.inventory.resources[Resource.COAL])
        assertEquals(0, updatedRobot.inventory.resources[Resource.IRON])
        assertEquals(0, updatedRobot.inventory.resources[Resource.GOLD])
        assertEquals(0, updatedRobot.inventory.resources[Resource.GEM])
        assertEquals(0, updatedRobot.inventory.resources[Resource.PLATIN])

    }

}