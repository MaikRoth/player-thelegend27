package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.planet.domain.DiscoveredPlanet
import com.example.thelegend27.planet.domain.PlanetRepository
import com.example.thelegend27.planet.domainprimitives.DiscoveredDeposit
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.TestHelper
import com.example.thelegend27.robot.domain.strategies.ExploreStrategy
import com.example.thelegend27.robot.domain.strategies.farming.FarmStrategy
import com.example.thelegend27.robot.domain.strategies.fighting.FightStrategy
import com.example.thelegend27.trading.domain.Resource
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import java.util.*

class RobotSpawnedHandlerTest {
    val robotSpawnedHandler = RobotSpawnedHandler()
    private var OPTIMAL_PERCENTAGE_OF_FARMERS = 0.65
    private var OPTIMAL_PERCENTAGE_OF_COAL_FARMERS = 0.5906
    private var OPTIMAL_PERCENTAGE_OF_IRON_FARMERS = 0.0729
    private var OPTIMAL_PERCENTAGE_OF_GEM_FARMERS = 0.02
    private var OPTIMAL_PERCENTAGE_OF_GOLD_FARMERS = 0.01
    private var OPTIMAL_PERCENTAGE_OF_PLATIN_FARMERS = 0.0036
    private var MAXIMUM_AMOUNT_OF_EXPLORERS = 5

    private val testHelper = TestHelper()

    @BeforeEach
    fun setUp() {
        RobotRepository.clear()
        PlanetRepository.clear()
    }

    @Test
    fun testHandle() {
        runBlocking {
            val spawnedEvent = testHelper.createSpawnedEvent()
            robotSpawnedHandler.handle(spawnedEvent.eventBody)
            assertTrue(1 == RobotRepository.getAll().size)
            assertTrue(RobotRepository.getAll()[0].strategy is FarmStrategy)
            assertTrue(RobotRepository.getAll()[0].currentStatus.health == spawnedEvent.eventBody.robot.health)
            assertTrue(RobotRepository.getAll()[0].currentStatus.energy == spawnedEvent.eventBody.robot.energy)
            assertTrue(RobotRepository.getAll()[0].currentPlanet.id== UUID.fromString(spawnedEvent.eventBody.robot.planet.planetId))
            assertTrue(RobotRepository.getAll()[0].id == UUID.fromString(spawnedEvent.eventBody.robot.robotId))
            assertTrue(PlanetRepository.getAll().size == 1)
            assertTrue(PlanetRepository.getAll()[0].id == UUID.fromString(spawnedEvent.eventBody.robot.planet.planetId))

            for (i in 0..500) {
                val spawnedEvent2 = testHelper.createSpawnedEvent()
                robotSpawnedHandler.handle(spawnedEvent2.eventBody)
            }
            assertEquals(502, RobotRepository.getAll().size)

            val amountOfFarmers = RobotRepository.getAll().filter { it.strategy is FarmStrategy }.size
            val amountOfExplorers = RobotRepository.getAll().filter { it.strategy is ExploreStrategy }.size
            val amountOfCoalFarmers = RobotRepository.getAll().filter { it.strategy is FarmStrategy && (it.strategy as FarmStrategy).resourceToBeMined == Resource.COAL }.size
            val amountOfIronFarmers = RobotRepository.getAll().filter { it.strategy is FarmStrategy && (it.strategy as FarmStrategy).resourceToBeMined == Resource.IRON }.size
            val amountOfGoldFarmers = RobotRepository.getAll().filter { it.strategy is FarmStrategy && (it.strategy as FarmStrategy).resourceToBeMined == Resource.GOLD }.size
            val amountOfGemFarmers = RobotRepository.getAll().filter { it.strategy is FarmStrategy && (it.strategy as FarmStrategy).resourceToBeMined == Resource.GEM }.size
            val amountOfPlatinFarmers = RobotRepository.getAll().filter { it.strategy is FarmStrategy && (it.strategy as FarmStrategy).resourceToBeMined == Resource.PLATIN }.size
            val amountOfFighters = RobotRepository.getAll().filter { it.strategy is FightStrategy }.size

            assertTrue((amountOfFarmers.toDouble()/(amountOfFarmers+amountOfExplorers+amountOfFighters).toDouble()*100.0) >= OPTIMAL_PERCENTAGE_OF_FARMERS * 100)
            assertEquals(amountOfFarmers, amountOfCoalFarmers + amountOfIronFarmers + amountOfGoldFarmers + amountOfGemFarmers + amountOfPlatinFarmers)
            assertTrue(amountOfCoalFarmers >= OPTIMAL_PERCENTAGE_OF_COAL_FARMERS * amountOfFarmers)
            assertTrue(amountOfIronFarmers >= OPTIMAL_PERCENTAGE_OF_IRON_FARMERS * amountOfFarmers)
            assertTrue(amountOfGoldFarmers >= OPTIMAL_PERCENTAGE_OF_GOLD_FARMERS * amountOfFarmers)
            assertTrue(amountOfGemFarmers >= OPTIMAL_PERCENTAGE_OF_GEM_FARMERS * amountOfFarmers)
            assertTrue(amountOfPlatinFarmers >= OPTIMAL_PERCENTAGE_OF_PLATIN_FARMERS * amountOfFarmers)
            assertTrue(amountOfExplorers <= MAXIMUM_AMOUNT_OF_EXPLORERS)


        }
    }
}