package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.RobotUpgradedEvent
import com.example.thelegend27.robot.domain.RobotRepository
import com.example.thelegend27.robot.domain.TestHelper
import com.example.thelegend27.trading.domain.Upgrade
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*


class RobotUpgradedHandlerTest {
    val testHelper = TestHelper()
    @Test
    fun `handle should upgrade the robot and log the event`() = runBlocking {
        // Arrange
        val robot = testHelper.getRobot()
        var event = RobotUpgradedEvent(robot.id.toString(), Upgrade.DAMAGE, 2)

        RobotRepository.addOrReplace(robot)

        val handler = RobotUpgradedHandler()

        // Act
        handler.handle(event)

        // Assert
        val updatedRobot = RobotRepository.get(robot.id).getOrNull()!!

        // Verify that the robot was upgraded only 1 level
        assertEquals(1, updatedRobot.levels.damageLevel)

        // try upgrading the robot over the max level (5)
        val currentLevel = updatedRobot.levels.damageLevel
        for (i in 1..5) {
            event = RobotUpgradedEvent(robot.id.toString(), Upgrade.DAMAGE, currentLevel + i)
            try {
                handler.handle(event)
            } catch (e: Exception) {
                assertThrows(IllegalArgumentException::class.java) {
                    throw e
                }
            }
        }
        // Verify that the robot was upgraded only 5 levels
        assertEquals(5, updatedRobot.levels.damageLevel)

    }

}