package com.example.thelegend27.robot.application.usecases

import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotsRevealedIntegrationEvent
import com.example.thelegend27.robot.domain.EnemyRobotRepository
import com.example.thelegend27.robot.domain.TestHelper
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class RobotsRevealedHandlerTest {
    val testHelper = TestHelper()
    @Test
    fun handle() {
        // Create a test event with two robots
        val robot1 = testHelper.getRobotRevealedDto()
        val robot2 = testHelper.getRobotRevealedDto()
        val event = RobotsRevealedIntegrationEvent(listOf(robot1, robot2))

        // Add one robot to the repository to simulate an existing robot
        val existingRobot = testHelper.getEnemyRobot()
        EnemyRobotRepository.add(existingRobot)

        // Call the handle method of the handler under test
        runBlocking { RobotsRevealedHandler().handle(event) }

        // Verify that two new robots were added to the repository
        val newRobots = EnemyRobotRepository.getAll().filter { robot ->
            robot.robotId != existingRobot.robotId}

        assertEquals(2, newRobots.size)

        // Verify that the existing robot was removed
        assertFalse(EnemyRobotRepository.containsKey(existingRobot.robotId))

        // Verify that the repository contains the correct amount of robots
        assertEquals(2, EnemyRobotRepository.getSize())

    }
}