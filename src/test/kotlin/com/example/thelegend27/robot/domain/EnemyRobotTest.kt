package com.example.thelegend27.robot.domain

import com.example.thelegend27.robot.domainprimitives.RobotLevels
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class EnemyRobotTest {

    @Test
    fun `test calculateFightingScore`() {
        val robot = EnemyRobot(UUID.randomUUID(), RobotLevels(2, 3, 4), UUID.randomUUID(), "player1", 100, 50)
        assertEquals(77.0, robot.calculateFightingScore())
    }

    @Test
    fun `test decreaseHealthBy`() = runBlocking {
        val robot = EnemyRobot(UUID.randomUUID(), RobotLevels(), UUID.randomUUID(), "player1", 100, 50)
        robot.decreaseHealthBy(20)
        assertEquals(80, robot.health)
        robot.decreaseHealthBy(200)
        assertEquals(0, robot.health)
    }

    @Test
    fun `test getRobotId`() {
        val id = UUID.randomUUID()
        val robot = EnemyRobot(id, RobotLevels(), UUID.randomUUID(), "player1", 100, 50)
        assertEquals(id, robot.robotId)
    }

    @Test
    fun `test getLevels`() {
        val levels = RobotLevels(2, 3, 4)
        val robot = EnemyRobot(UUID.randomUUID(), levels, UUID.randomUUID(), "player1", 100, 50)
        assertEquals(levels, robot.levels)
    }

    @Test
    fun `test getPlanetId`() {
        val id = UUID.randomUUID()
        val robot = EnemyRobot(UUID.randomUUID(), RobotLevels(), id, "player1", 100, 50)
        assertEquals(id, robot.planetId)
    }

    @Test
    fun `test getPlayerNotion`() {
        val notion = "player1"
        val robot = EnemyRobot(UUID.randomUUID(), RobotLevels(), UUID.randomUUID(), notion, 100, 50)
        assertEquals(notion, robot.playerNotion)
    }

    @Test
    fun `test getHealth`() {
        val health = 100
        val robot = EnemyRobot(UUID.randomUUID(), RobotLevels(), UUID.randomUUID(), "player1", health, 50)
        assertEquals(health, robot.health)
    }

    @Test
    fun `test setHealth`() {
        val robot = EnemyRobot(UUID.randomUUID(), RobotLevels(), UUID.randomUUID(), "player1", 100, 50)
        robot.health = 80
        assertEquals(80, robot.health)
    }

    @Test
    fun `test getEnergy`() {
        val energy = 50
        val robot = EnemyRobot(UUID.randomUUID(), RobotLevels(), UUID.randomUUID(), "player1", 100, energy)
        assertEquals(energy, robot.energy)
    }

    @Test
    fun `test setEnergy`() {
        val robot = EnemyRobot(UUID.randomUUID(), RobotLevels(), UUID.randomUUID(), "player1", 100, 50)
        robot.energy = 80
        assertEquals(80, robot.energy)
    }
}
