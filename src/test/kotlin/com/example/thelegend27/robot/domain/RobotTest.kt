package com.example.thelegend27.robot.domain

import com.example.thelegend27.planet.domain.DiscoveredPlanet
import com.example.thelegend27.planet.domainprimitives.DiscoveredDeposit
import com.example.thelegend27.robot.domain.strategies.farming.FarmStrategy
import com.example.thelegend27.robot.domainprimitives.CurrentStatus
import com.example.thelegend27.trading.domain.Resource
import com.example.thelegend27.trading.domain.Upgrade
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*

class RobotTest {
    private val id = UUID.randomUUID()
    private val planet = DiscoveredPlanet(UUID.randomUUID(), gameWorldId = null, deposit = DiscoveredDeposit(Resource.IRON, 10000,10000), clusterId = UUID.randomUUID())
    private val robot = Robot(id, planet)

    @Test
    fun testGettersAndSetters() {
        assertTrue(robot.alive)

        val strategy = robot.strategy
        assertNotNull(strategy)
        assertTrue(strategy is FarmStrategy)

        val levels = robot.levels
        assertNotNull(levels)
        assertTrue(levels.isLevelZero())

        val currentStatus = robot.currentStatus
        assertNotNull(currentStatus)
        assertEquals(10, currentStatus.health)
        assertEquals(20, currentStatus.energy)

        val inventory = robot.inventory
        assertNotNull(inventory)
        assertEquals(20, inventory.maxStorage)
        assertEquals(0, inventory.usedStorage)

        robot.alive = false
        assertFalse(robot.alive)

        assertEquals(0.0, robot.calculateFightingScore())

    }

    @Test
    fun testLevelUp() {
        val initialHealthLevel = robot.levels.healthLevel
        robot.levelUp(Upgrade.HEALTH)
        assertEquals(initialHealthLevel + 1, robot.levels.healthLevel)

        val initialStorageLevel = robot.levels.storageLevel
        robot.levelUp(Upgrade.STORAGE)
        assertEquals(initialStorageLevel + 1, robot.levels.storageLevel)

        val initialDamageLevel = robot.levels.damageLevel
        robot.levelUp(Upgrade.DAMAGE)
        assertEquals(initialDamageLevel + 1, robot.levels.damageLevel)

        val initialEnergyLevel = robot.levels.energyLevel
        robot.levelUp(Upgrade.MAX_ENERGY)
        assertEquals(initialEnergyLevel + 1, robot.levels.energyLevel)

        val initialEnergyRegenLevel = robot.levels.energyRegenLevel
        robot.levelUp(Upgrade.ENERGY_REGEN)
        assertEquals(initialEnergyRegenLevel + 1, robot.levels.energyRegenLevel)

        val initialMiningLevel = robot.levels.miningLevel
        robot.levelUp(Upgrade.MINING)
        assertEquals(initialMiningLevel + 1, robot.levels.miningLevel)

        val initialMiningSpeedLevel = robot.levels.miningSpeedLevel
        robot.levelUp(Upgrade.MINING_SPEED)
        assertEquals(initialMiningSpeedLevel + 1, robot.levels.miningSpeedLevel)
    }

    @Test
    fun testCalculateFightingScore() {
        robot.levels.healthLevel = 2
        robot.levels.damageLevel = 3
        robot.levels.energyLevel = 4
        robot.currentStatus = CurrentStatus(10, 5)

        assertEquals(10.5, robot.calculateFightingScore())
    }

    @Test
    fun testIdAndCurrentPlanet() {

        assertEquals(id, robot.id)
        assertEquals(planet, robot.currentPlanet)

        val newPlanet = DiscoveredPlanet(UUID.randomUUID(), gameWorldId = null, deposit = DiscoveredDeposit(Resource.IRON, 10000,10000), clusterId = UUID.randomUUID())
        robot.currentPlanet = newPlanet
        assertEquals(newPlanet, robot.currentPlanet)
    }
}
