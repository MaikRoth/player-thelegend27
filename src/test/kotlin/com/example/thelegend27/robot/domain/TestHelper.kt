package com.example.thelegend27.robot.domain

import com.example.thelegend27.eventinfrastructure.Event
import com.example.thelegend27.eventinfrastructure.EventHeaderDto
import com.example.thelegend27.eventinfrastructure.map.PlanetDto
import com.example.thelegend27.eventinfrastructure.robot.*
import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotResourceMinedIntegrationEvent
import com.example.thelegend27.eventinfrastructure.robot.integrationevents.RobotRevealedDTO
import com.example.thelegend27.planet.domain.DiscoveredPlanet
import com.example.thelegend27.planet.domain.Planet
import com.example.thelegend27.planet.domain.PlanetRepository
import com.example.thelegend27.planet.domainprimitives.DiscoveredDeposit
import com.example.thelegend27.robot.domainprimitives.CurrentStatus
import com.example.thelegend27.robot.domainprimitives.Inventory
import com.example.thelegend27.robot.domainprimitives.RobotLevels
import com.example.thelegend27.robot.domainprimitives.RobotStats
import com.example.thelegend27.trading.domain.Resource
import com.example.thelegend27.trading.domain.Upgrade
import java.util.*

class TestHelper {

    fun getRobotEvent(): RobotDto {
        return RobotDto(
            PlanetDto(UUID.randomUUID().toString(), "1", 1, "coal"),
            RobotInventoryDto(0, 0, 20, false, ResourceInventoryDto()),
            UUID.randomUUID().toString(),
            true,
            "TheLegend27",
            10,
            20,
            4,
            1,
            2,
            10,
            20,
            0,
            0,
            0,
            0,
            0,
            0,
        )
    }

    fun getRobotRevealedDto(): RobotRevealedDTO {
        return RobotRevealedDTO(
            RobotLevels(1, 1, 1, 1, 1, 1, 1),
            UUID.randomUUID().toString(),
            "TheLegend27",
            10,
            20,
            UUID.randomUUID().toString()
        )
    }

    fun getEnemyRobot(): EnemyRobot {
        return EnemyRobot(
            UUID.randomUUID(),
            RobotLevels(1, 1, 1, 1, 1, 1, 1),
            UUID.randomUUID(),
            "TheLegend27",
            10,
            20
        )
    }
    fun createSpawnedEvent(): Event<RobotSpawnedEvent> {
        val eventHeader =
            EventHeaderDto(
                UUID.randomUUID().toString(),
                "{}",
                "${UUID.randomUUID()}",
                "robotSpawned",
                "42",
                "TheLegend27"
            )
        val robotEvent = getRobotEvent()
        PlanetRepository.add(discoveredPlanetFromPlanetDto(robotEvent.planet))
        return Event(eventHeader, RobotSpawnedEvent(robotEvent))
    }

    fun addToPlanetRepository(planet: DiscoveredPlanet) {
        PlanetRepository.add(planet)
    }

    fun createRobotMovedEvent(robot: String, currentPlanet: String, destinationPlanet: String): RobotMovedEvent {
        return RobotMovedEvent(robot, currentPlanet, destinationPlanet)
    }

    fun createRobotAttackedEvent() {
        TODO()
    }

    fun createRobotKilledEvent(robot: String): RobotKilledEvent {
        return RobotKilledEvent(robot)
    }

    fun createRobotHealthUpdatedEvent(robot: String, health: Int): RobotHealthUpdatedEvent {
        return RobotHealthUpdatedEvent(robot, 1, health)
    }

    fun createRobotEnergyUpdatedEvent(robot: String, energy: Int): RobotEnergyUpdatedEvent {
        return RobotEnergyUpdatedEvent(robot, 0, energy)
    }

    fun createRobotResourceMinedEvent(robot: String, resource: String, amount: Int): RobotResourceMinedEvent {
        return RobotResourceMinedEvent(robot, amount, resource)
    }
    fun discoveredPlanetFromPlanetDto(planetDto: PlanetDto): DiscoveredPlanet {
        return DiscoveredPlanet(UUID.fromString(planetDto.planetId), deposit = DiscoveredDeposit(Resource.COAL,10000,10000), clusterId = UUID.randomUUID(), gameWorldId = UUID.randomUUID())
    }

    fun getRobotFromDto(dto: RobotDto): Robot {
        val planet = discoveredPlanetFromPlanetDto(dto.planet)

        val levels = RobotLevels(
            dto.healthLevel,
            dto.energyLevel,
            dto.energyRegenLevel,
            dto.damageLevel,
            dto.miningSpeedLevel,
            dto.miningLevel,
            0,
        )
        val currentStatus = CurrentStatus(dto.health, dto.energy)
        val robot = Robot(UUID.fromString(dto.robotId), planet)
        robot.currentStatus = currentStatus
        robot.inventory = Inventory.fromMaxStorage(dto.inventory.maxStorage)
        robot.levels = levels
        robot.alive = dto.alive
        return robot
    }

    fun createDiscoveredPlanet(): DiscoveredPlanet {
        return DiscoveredPlanet(UUID.randomUUID(), deposit = DiscoveredDeposit(Resource.COAL,10000,10000), clusterId = UUID.randomUUID(), gameWorldId = UUID.randomUUID())
    }

    fun inventoryFilledWithResources(): Inventory {
        return Inventory.fromMaxStorage(99999).fromInventoryAndRobotResourceMined(RobotResourceMinedIntegrationEvent(UUID.randomUUID().toString(), 1, Resource.COAL, ResourceInventoryDto(20,32,34,23,433)))
    }
    fun getRobot (): Robot {
        return getRobotFromDto(getRobotEvent())
    }
}