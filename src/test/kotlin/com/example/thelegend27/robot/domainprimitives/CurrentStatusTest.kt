package com.example.thelegend27.robot.domainprimitives

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class CurrentStatusTest {

    @Test
    fun getHealth() {
        val status = CurrentStatus(10, 20)
        assertEquals(10, status.health)
    }

    @Test
    fun getEnergy() {
        val status = CurrentStatus(10, 20)
        assertEquals(20, status.energy)
    }

    @Test
    fun testHashCode() {
        val status1 = CurrentStatus(10, 20)
        val status2 = CurrentStatus(10, 20)
        assertEquals(status1.hashCode(), status2.hashCode())
    }

    @Test
    fun testEquals() {
        val status1 = CurrentStatus(10, 20)
        val status2 = CurrentStatus(10, 20)
        val status3 = CurrentStatus(5, 20)
        assertTrue(status1 == status2)
        assertFalse(status1 == status3)
    }
}
