package com.example.thelegend27.robot.domainprimitives

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

class RobotLevelsTest {

    @Test
    fun sumOfLevels() {
        val robotLevels = RobotLevels(healthLevel = 2, damageLevel = 3, miningSpeedLevel = 1)
        assertEquals(6, robotLevels.sumOfLevels())
    }

    @Test
    fun upgradeHealth() {
        val robotLevels = RobotLevels(healthLevel = 2)
        robotLevels.upgradeHealth()
        assertEquals(3, robotLevels.healthLevel)
    }

    @Test
    fun createLevelOverMaxLevel() {
        try {
            RobotLevels(6)
        }
        catch (e: IllegalArgumentException) {
            assertEquals("Level cannot be greater than 5", e.message)
        }
    }

    @Test
    fun upgradeMiningSpeed() {
        val robotLevels = RobotLevels(miningSpeedLevel = 5)
        assertThrows(IllegalArgumentException::class.java) {
            robotLevels.upgradeMiningSpeed()
        }
    }

    @Test
    fun upgradeMining() {
        val robotLevels = RobotLevels(miningLevel = 3)
        robotLevels.upgradeMining()
        assertEquals(4, robotLevels.miningLevel)
    }

    @Test
    fun upgradeEnergy() {
        val robotLevels = RobotLevels(energyLevel = 1)
        robotLevels.upgradeEnergy()
        assertEquals(2, robotLevels.energyLevel)
    }

    @Test
    fun upgradeEnergyRegen() {
        val robotLevels = RobotLevels(energyRegenLevel = 0)
        robotLevels.upgradeEnergyRegen()
        assertEquals(1, robotLevels.energyRegenLevel)
    }

    @Test
    fun upgradeStorage() {
        val robotLevels = RobotLevels(storageLevel = 2)
        robotLevels.upgradeStorage()
        assertEquals(3, robotLevels.storageLevel)
    }

    @Test
    fun isLevelZero() {
        val robotLevels = RobotLevels()
        assertTrue(robotLevels.isLevelZero())
        robotLevels.upgradeHealth()
        assertFalse(robotLevels.isLevelZero())
    }

    @Test
    fun copy() {
        val robotLevels = RobotLevels(healthLevel = 2, damageLevel = 3)
        val copiedRobotLevels = robotLevels.copy(miningLevel = 1)
        assertEquals(RobotLevels(healthLevel = 2, damageLevel = 3, miningLevel = 1), copiedRobotLevels)
    }

    @Test
    fun testEquals() {
        val robotLevels1 = RobotLevels(healthLevel = 2, damageLevel = 3)
        val robotLevels2 = RobotLevels(healthLevel = 2, damageLevel = 3)
        val robotLevels3 = RobotLevels(healthLevel = 1, damageLevel = 3)

        assertTrue(robotLevels1 == robotLevels2)
        assertFalse(robotLevels1 == robotLevels3)
    }
}
