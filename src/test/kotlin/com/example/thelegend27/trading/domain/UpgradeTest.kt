package com.example.thelegend27.trading.domain

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class UpgradeTest {

    @Test
    fun testGetValue() {
        assertEquals(20, Upgrade.STORAGE.getValue(0))
        assertEquals(50, Upgrade.STORAGE.getValue(1))
        assertEquals(100, Upgrade.STORAGE.getValue(2))
        assertEquals(200, Upgrade.STORAGE.getValue(3))
        assertEquals(400, Upgrade.STORAGE.getValue(4))
        assertEquals(1000, Upgrade.STORAGE.getValue(5))

        assertEquals(10, Upgrade.HEALTH.getValue(0))
        assertEquals(25, Upgrade.HEALTH.getValue(1))
        assertEquals(50, Upgrade.HEALTH.getValue(2))
        assertEquals(100, Upgrade.HEALTH.getValue(3))
        assertEquals(200, Upgrade.HEALTH.getValue(4))
        assertEquals(500, Upgrade.HEALTH.getValue(5))

        assertEquals(1, Upgrade.DAMAGE.getValue(0))
        assertEquals(2, Upgrade.DAMAGE.getValue(1))
        assertEquals(5, Upgrade.DAMAGE.getValue(2))
        assertEquals(10, Upgrade.DAMAGE.getValue(3))
        assertEquals(20, Upgrade.DAMAGE.getValue(4))
        assertEquals(50, Upgrade.DAMAGE.getValue(5))

        assertEquals(2, Upgrade.MINING_SPEED.getValue(0))
        assertEquals(5, Upgrade.MINING_SPEED.getValue(1))
        assertEquals(10, Upgrade.MINING_SPEED.getValue(2))
        assertEquals(15, Upgrade.MINING_SPEED.getValue(3))
        assertEquals(20, Upgrade.MINING_SPEED.getValue(4))
        assertEquals(40, Upgrade.MINING_SPEED.getValue(5))

        assertEquals(2, Upgrade.MINING.getValue(0))
        assertEquals(3, Upgrade.MINING.getValue(1))
        assertEquals(4, Upgrade.MINING.getValue(2))
        assertEquals(5, Upgrade.MINING.getValue(3))
        assertEquals(6, Upgrade.MINING.getValue(4))
        assertEquals(7, Upgrade.MINING.getValue(5))

        assertEquals(20, Upgrade.MAX_ENERGY.getValue(0))
        assertEquals(30, Upgrade.MAX_ENERGY.getValue(1))
        assertEquals(40, Upgrade.MAX_ENERGY.getValue(2))
        assertEquals(60, Upgrade.MAX_ENERGY.getValue(3))
        assertEquals(100, Upgrade.MAX_ENERGY.getValue(4))
        assertEquals(200, Upgrade.MAX_ENERGY.getValue(5))

        assertEquals(4, Upgrade.ENERGY_REGEN.getValue(0))
        assertEquals(6, Upgrade.ENERGY_REGEN.getValue(1))
        assertEquals(8, Upgrade.ENERGY_REGEN.getValue(2))
        assertEquals(10, Upgrade.ENERGY_REGEN.getValue(3))
        assertEquals(15, Upgrade.ENERGY_REGEN.getValue(4))
        assertEquals(20, Upgrade.ENERGY_REGEN.getValue(5))

        assertThrows(IllegalArgumentException::class.java) {
            Upgrade.STORAGE.getValue(-1)
        }
    }

    @Test
    fun getPrice() {
        val upgrade = Upgrade.DAMAGE
        assertEquals(50, upgrade.getPrice(1))
        assertEquals(300, upgrade.getPrice(2))
        assertEquals(1500, upgrade.getPrice(3))
        assertEquals(4000, upgrade.getPrice(4))
        assertEquals(15000, upgrade.getPrice(5))
        assertThrows(Exception::class.java) { upgrade.getPrice(0) }
    }

    @Test
    fun values() {
        val values = Upgrade.values()
        assertEquals(7, values.size)
        assertTrue(values.contains(Upgrade.STORAGE))
        assertTrue(values.contains(Upgrade.HEALTH))
        assertTrue(values.contains(Upgrade.DAMAGE))
        assertTrue(values.contains(Upgrade.MINING_SPEED))
        assertTrue(values.contains(Upgrade.MINING))
        assertTrue(values.contains(Upgrade.MAX_ENERGY))
        assertTrue(values.contains(Upgrade.ENERGY_REGEN))
    }

    @Test
    fun valueOf() {
        assertEquals(Upgrade.STORAGE, Upgrade.valueOf("STORAGE"))
        assertEquals(Upgrade.HEALTH, Upgrade.valueOf("HEALTH"))
        assertEquals(Upgrade.DAMAGE, Upgrade.valueOf("DAMAGE"))
        assertEquals(Upgrade.MINING_SPEED, Upgrade.valueOf("MINING_SPEED"))
        assertEquals(Upgrade.MINING, Upgrade.valueOf("MINING"))
        assertEquals(Upgrade.MAX_ENERGY, Upgrade.valueOf("MAX_ENERGY"))
        assertEquals(Upgrade.ENERGY_REGEN, Upgrade.valueOf("ENERGY_REGEN"))
        assertThrows(IllegalArgumentException::class.java) { Upgrade.valueOf("INVALID") }
    }
}